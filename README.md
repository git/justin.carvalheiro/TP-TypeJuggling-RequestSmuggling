# TP Type Juggling et Request Smuggling 

## Mise en place 

Lancer VDN et cloner le dépôt git sur la machine virtuelle.\
Pensez à désactiver le proxy avec les commandes :  `unset http_proxy` et `unset https_proxy` 

## Exercice 1 : Type Juggling 

A la racine du dépôt, exécutez les commandes suivantes :\
`docker build -t juggling type_juggling/`\
`docker run -it juggling` 

Puis, depuis un navigateur, rendez-vous sur : http://127.17.0.2/index.php\
*Si l'adresse ne fonctionne pas, vérifiez l'adresse IP de votre conteneur Docker.*

Votre but est de vous connecter en tant qu'administrateur. 

> Note : Vous devez devinez la fonction de hachage utilisée

 ## Exercice 2 :  Request Smuggling 

### Mise en place 

A la racine du dépôt, exécutez les commandes suivantes :\
`docker-compose -f request_smuggling/docker-compose.yaml build`\
`docker-compose -f request_smuggling/docker-compose.yaml up` 

Le docker compose que vous venez d'exécuter, à créer deux serveurs dont un proxy. 

Gentil serveur qui héberge l'application web cible : `128.11.0.5`\
Proxy très sécurisé qui bloque les méchants attaquants : `128.11.0.6`

### Exercice 

Votre mission si vous l'acceptez, est de récupérer le message très secret contenu dans le fichier : `secret_file.txt` 

Pour cela vous devez construire une requête HTTP qui vous permettra de contourner la sécurité du proxy.\
Vous trouverez dans le dépôt un script python qui vous aidera dans la construction de votre requête.
