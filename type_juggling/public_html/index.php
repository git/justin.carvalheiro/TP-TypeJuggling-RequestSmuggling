<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>

<h2>Connexion</h2>

<form method="post" action="">
    <label for="username">Nom d'utilisateur :</label>
    <input type="text" id="username" name="username" required><br><br>

    <label for="password">Mot de passe :</label>
    <input type="password" id="password" name="password" required><br><br>

    <input type="submit" value="Se connecter">
</form>

</body>
</html>

<?php
function readUsersFile($filename) {
    $users = [];
    $file = fopen($filename, "r");
    if ($file) {
        while (!feof($file)) {
            $line = fgets($file);
            list($username, $password) = explode(':', trim($line));
            $users[$username] = $password;
        }
        fclose($file);
    }
    return $users;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $users = readUsersFile('users.txt.lock');
    if (isset($users[$username]) && $users[$username] == md5($password) ){
        echo "Bienvenue, $username ! Vous êtes connecté.";
    } else {
        echo "Identifiants invalides. Veuillez réessayer.";
    }
}

?>
